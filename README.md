# react-native-radaeepdf

React Native RadaeePDF for Android, iOS, &amp; Windows

## Installation

```sh
npm install react-native-radaeepdf
```

## Usage

```js
import Radaeepdf from "react-native-radaeepdf";

// ...

const deviceName = await Radaeepdf.getDeviceName();
```

## License

MIT
